import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:map_app/bloc/map/map_bloc.dart';
import 'package:map_app/bloc/my_location/my_location_bloc.dart';
import 'package:map_app/widgets/widgets.dart';


class MapPage extends StatefulWidget {

  @override
  _MapPageState createState() => _MapPageState();
}

class _MapPageState extends State<MapPage> {

  @override
  void initState() { 
    context.read<MyLocationBloc>().startTracking();
    super.initState();
    
  }

  @override
  void dispose() {
    context.read<MyLocationBloc>().cancelTracking();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: BlocBuilder<MyLocationBloc, MyLocationState>(
        builder: (context, state) => createMap(state),
      ),
      floatingActionButton: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          ButtonLocation(),
        ],
      ),
   );
  }

  Widget createMap(MyLocationState state){
    if(! state.locationExists) return Center(child: Text('locating'));

    final mapBloc = BlocProvider.of<MapBloc>(context);

    final cameraPosition = CameraPosition(
      target: state.location,
      zoom: 15
    );
    return GoogleMap(
      initialCameraPosition: cameraPosition,
      mapType: MapType.normal,
      myLocationEnabled: true,
      myLocationButtonEnabled: false,
      zoomControlsEnabled: false,
      onMapCreated: (GoogleMapController controller){
        mapBloc.initMap(controller);
      },
      
    );
  }
}