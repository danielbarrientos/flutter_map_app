import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart' as Geolocator;
import 'package:map_app/helpers/helpers.dart';
import 'package:map_app/pages/gps_access_page.dart';
import 'package:map_app/pages/map_page.dart';
import 'package:permission_handler/permission_handler.dart';


class LoadingPage extends StatefulWidget {

  

  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> with WidgetsBindingObserver{

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async{
    if(state == AppLifecycleState.resumed ){
      if( await Permission.location.isGranted) {
        Navigator.pushReplacementNamed(context, 'loading');
      }
    }
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: FutureBuilder(
        future: this.checkGpsLocation(context),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if(snapshot.hasData) {
            return Center(
              child: Text(snapshot.data)
            );
          }
          else{
            return Center(
              child: CircularProgressIndicator(strokeWidth: 3.0,),
            );
          }
        },
      ),
   );
  }

  Future checkGpsLocation( BuildContext context) async{

    //permission GPS
    final permissionGPS = await Permission.location.isGranted;
    //Gps active
    // ignore: deprecated_member_use
    final activeGPS = await Geolocator.isLocationServiceEnabled();

    if( permissionGPS && activeGPS){
       Navigator.pushReplacement(context, navigateMapFadeIn(context,MapPage()));
       return '';
    }else if(! permissionGPS){
      Navigator.pushReplacement(context, navigateMapFadeIn(context,GpsAccessPage()));
      return 'GPS permission is required';
    } else if(! activeGPS){
      return 'Activated GPS';
    }
    // await Future.delayed(Duration(milliseconds: 1000));
   
    // Navigator.pushReplacement(context, navigateMapFadeIn(context,GpsAccessPage()));
    // Navigator.pushReplacement(context, navigateMapFadeIn(context,MapPage()));
  }
}