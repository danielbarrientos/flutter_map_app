import 'package:flutter/material.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:geolocator/geolocator.dart' as Geolocator;

class GpsAccessPage extends StatefulWidget {

  @override
  _GpsAccessPageState createState() => _GpsAccessPageState();
}

class _GpsAccessPageState extends State<GpsAccessPage> with WidgetsBindingObserver {

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
  }

  @override
  void dispose() {
     WidgetsBinding.instance.removeObserver(this);
    super.dispose();
  }
  @override
  void didChangeAppLifecycleState(AppLifecycleState state) async{
    if(state == AppLifecycleState.resumed ){
      // ignore: deprecated_member_use
      if( await Geolocator.isLocationServiceEnabled()) {
        Navigator.pushReplacementNamed(context, 'map');
      }
    }
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('It is necessary to activate the gps'),
            MaterialButton(
              child: Text('Request access', style: TextStyle(color: Colors.white)),
              color: Colors.black,
              shape: StadiumBorder(),
              splashColor: Colors.transparent,
              elevation: 0,
              onPressed: () async{
                final status = await Permission.location.request();

                Future.delayed(Duration.zero, () {
                  this.accessGPS(status);
                });
              }
            )
          ],
        )
     ),
   );
  }

  void accessGPS(PermissionStatus status) {

    switch (status) {
      case PermissionStatus.granted:
          Navigator.pushReplacementNamed(context, 'map');
        break;
      case PermissionStatus.undetermined:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
      case PermissionStatus.permanentlyDenied:
        openAppSettings();
        break;
         
    }
  }
}